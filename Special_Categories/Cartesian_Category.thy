theory Cartesian_Category imports Binary_Product_Category "../Constructions/Equalizers"
                                                          "../Constructions/Finals_Initials"

begin

subsection \<open>Cartesian Categories\<close>

locale cartesian_category = binary_product_category + 
  fixes 
          equalizer :: "'a \<Rightarrow> 'a \<Rightarrow> 'a" (infix "\<doteq>" 120) and
          final_object :: "'a" ("\<one>")
  assumes 
          equalizer:    "is_parallel_pair f g \<longrightarrow> (f \<doteq> g) equalizer_of f g" and
          final_object: "is_final \<one>" and

          equalizer_implies_parallel_pair: "E (f \<doteq> g) \<longrightarrow> is_parallel_pair f g"
begin

  lemma "True"  oops
    
  lemma equalizer_map: "is_parallel_pair f g \<Longrightarrow> (f \<doteq> g):dom (f\<doteq>g)\<rightarrow>(dom f)" and 
                       "is_parallel_pair f g \<Longrightarrow> (f \<doteq> g):dom (f\<doteq>g)\<rightarrow>(dom g)"
    using equalizer unfolding equalizer_of_def equalizes_def arrow_def
    using local.composition_existence local.factors_uniquely_through_def fl by auto

subsection \<open>Construction of the final map\<close>

  text \<open>This lemma motivates the following definition\<close>
  lemma "\<forall>t. \<exists>!x. (is_object t \<longrightarrow> x:t\<rightarrow>\<one>) \<and> (\<not> is_object t \<longrightarrow> x = *)"
    using final_object dom_implies_existence is_final_def by smt
  
  definition final_map :: "'a \<Rightarrow> 'a" ("!\<^sub>1") where
    "!\<^sub>1 t \<equiv> (THE x::'a. (is_object t \<longrightarrow> x:t\<rightarrow>\<one>) \<and> (\<not> is_object t \<longrightarrow> x = *))"
    (* \<longrightarrow> perhaps use Free Logic THE operator JAR paper [Achtung: Fehler im Haupttext] *)
  
  lemma final_map[cat]: "is_object t \<Longrightarrow> !\<^sub>1 t:t\<rightarrow>\<one>" 
  proof -  
    assume "is_object t"
    hence 1: "!\<^sub>1 t = (THE x::'a. x:t\<rightarrow>\<one>)" using final_map_def by auto
    have "\<exists>!x. x:t\<rightarrow>\<one>" using is_final_def final_object \<open>is_object t\<close> by metis
    hence "!\<^sub>1 t:t\<rightarrow>\<one>" using 1 theI'[of "\<lambda>x. x:t\<rightarrow>\<one>"] by smt
    thus ?thesis by auto
  qed

  lemma final_map_implies_type[cat]: "E (!\<^sub>1 t) \<Longrightarrow> is_object t" 
    apply (rule contrapos_pp[of "E (!\<^sub>1 t)"]) using inexistent_element final_map_def by auto

subsection \<open>Properties of the final object\<close>

  lemma product_final[cat]: "is_object A \<longrightarrow> is_product \<one> \<leftarrow>!\<^sub>1 A\<midarrow> A \<midarrow>A\<rightarrow> A"
    using local.final_object apply (simp add: is_product_def is_wedge_def is_final_def)
    using final_map[of A] fl
    local.arrow_implies_is_object local.arrow_def local.associativity local.cod_implies_existence 
    local.composition_existence local.dom_implies_existence local.is_object_def local.left_compose_id 
    local.right_compose_id
    unfolding fl by metis (* takes long *)
  
  lemma isomorphic_to_product_with_final[cat]: "is_object A \<Longrightarrow> A isomorphic_to (\<one> \<^bold>\<times> A)"
  proof - 
    assume "is_object A"
    hence "is_product \<one> \<leftarrow>\<pi>\<^sub>1 \<one> A\<midarrow> (\<one> \<^bold>\<times> A) \<midarrow>\<pi>\<^sub>2 \<one> A\<rightarrow> A" 
      using \<open>is_object A\<close> local.product_of_types[of "\<one>" A] final_object is_final_def by auto
    moreover have "is_product \<one> \<leftarrow>!\<^sub>1 A\<midarrow> A \<midarrow>A\<rightarrow> A" 
      using \<open>is_object A\<close> product_final[of A] by auto
    ultimately show ?thesis using products_isomorphic[where ?A="\<one>" and ?P="\<one> \<^bold>\<times> A" and ?B ="A"
                                                      and ?Q=A and ?q1.0="!\<^sub>1 A" and ?q2.0 = A 
                                                      and ?p1.0="\<pi>\<^sub>1 \<one> A" and ?p2.0="\<pi>\<^sub>2 \<one> A"] 
    by blast
  qed 

subsection \<open>Concatentation and the final map\<close>
  
  lemma final_map_composition[cat]: "E s \<Longrightarrow> !\<^sub>1 (cod s) \<cdot> s \<simeq> !\<^sub>1 (dom s)"
  proof - 
    assume "E s"
    hence "is_object (cod s) \<and> is_object (dom s)" 
      by (metis composition_existence local.is_object_def local.left_compose_id 
          local.right_compose_id fl)
    hence "!\<^sub>1 (cod s) \<cdot> s:(dom s)\<rightarrow>\<one>"
      using final_map[of "cod s"] unfolding arrow_def by (smt fl cat_ax)
    thus ?thesis 
      using final_object unfolding is_final_def using final_map cat_ax 
      by (smt fl \<open>is_object (cod s) \<and> is_object (dom s)\<close> local.products_isomorphism product_final) 
  qed

end

end
theory Binary_Product_Category imports "../Constructions/Products" "../Constructions/Monics_Epics"

begin

subsection \<open>Categories with binary products\<close>


locale binary_product_category = category +
    fixes product :: "'a \<Rightarrow> 'a \<Rightarrow> 'a" (infix "\<^bold>\<times>" 120) and
          projection1 :: "'a \<Rightarrow> 'a \<Rightarrow> 'a" ("\<pi>\<^sub>1 _ _") and
          projection2 :: "'a \<Rightarrow> 'a \<Rightarrow> 'a" ("\<pi>\<^sub>2 _ _") 
  assumes 
          product_of_types[cat]:      "is_object A \<and> is_object B \<longrightarrow> 
                                       is_product A \<leftarrow> (\<pi>\<^sub>1 A B) \<midarrow> (A \<^bold>\<times> B) \<midarrow> (\<pi>\<^sub>2 A B) \<rightarrow> B" and

          product_existence[cat]:     "E (A \<^bold>\<times> B) \<longrightarrow> (is_object A \<and> is_object B)" and
          projection1_existence[cat]: "E (\<pi>\<^sub>1 A B) \<longrightarrow> (is_object A \<and> is_object B)" and
          projection2_existence[cat]: "E (\<pi>\<^sub>2 A B) \<longrightarrow> (is_object A \<and> is_object B)"


(* locale unique_binary_product_category = binary_product_category +
  (* whatever assumption fits *)
  assumes "\<pi>\<^sub>1 A B = \<pi>\<^sub>1 C D \<longleftrightarrow> A\<^bold>\<times>B = C\<^bold>\<times>D" *)

context binary_product_category
begin

  lemma "True" nitpick[satisfy, card=4] oops
  
  subsection \<open>Basic statements on the chosen product\<close>
  
    lemma isomorphic_to_chosen_product:
      assumes "is_product A \<leftarrow>p1\<midarrow> P \<midarrow>p2\<rightarrow> B"
        shows "P isomorphic_to (A \<^bold>\<times> B)"
      by (smt assms local.arrow_implies_is_object local.is_product_def local.is_wedge_def 
          product_of_types local.products_isomorphic)
    
    lemma product_switch:
      assumes "is_product A \<leftarrow>p1\<midarrow> (A \<^bold>\<times> B) \<midarrow>p2\<rightarrow> B"
        shows "is_product B \<leftarrow>p2\<midarrow> (A \<^bold>\<times> B) \<midarrow>p1\<rightarrow> A"
      using assms unfolding pdefs by blast
    
    lemma product_is_object: "is_object A \<and> is_object B \<longleftrightarrow> is_object (A \<^bold>\<times> B)"
      by (meson fl isomorphic_to_chosen_product local.arrow_implies_is_object local.is_object_def 
          local.isomorphic_to_def local.isomorphism_arrow_def local.product_existence 
          product_of_types)
    
    lemma product_exists_implies_is_object: "E (A \<^bold>\<times> B) \<longrightarrow> is_object (A \<^bold>\<times> B)"
      by (meson isomorphic_to_chosen_product local.arrow_implies_is_object local.is_object_def 
          local.isomorphic_to_def local.isomorphism_arrow_def local.product_existence 
          product_of_types)
    
    lemma projection1_map: "is_object A \<longrightarrow> is_object B \<longrightarrow> (\<pi>\<^sub>1 A B):(A \<^bold>\<times> B)\<rightarrow>A"
      using local.product_of_types is_product_def is_wedge_def by auto
    
    lemma projection2_map: "is_object A \<longrightarrow> is_object B \<longrightarrow> (\<pi>\<^sub>2 A B):(A \<^bold>\<times> B)\<rightarrow>B"
      using local.product_of_types is_product_def is_wedge_def by auto
    
    lemma isomorphic_to_switch: "is_object A \<and> is_object B \<Longrightarrow> (A \<^bold>\<times> B) isomorphic_to (B \<^bold>\<times> A)"
      by (meson local.product_of_types local.products_isomorphic product_switch)
    
    lemma products_equal_factors_equal: "A \<^bold>\<times> B \<simeq> C \<^bold>\<times> D \<longrightarrow> A \<simeq> C \<and> B \<simeq> D" 
      oops (* Does not hold anymore. Old proof: *)
  (*     by (metis existing_identity_def local.arrow_def local.product_existence projection1_map 
        projection2_map) *)
        
    lemma ternary_product_associative:
      assumes "is_object A" and "is_object B" and "is_object C"
      shows "((A \<^bold>\<times> B) \<^bold>\<times> C) isomorphic_to (A \<^bold>\<times> (B \<^bold>\<times> C))"
      oops

subsection \<open>Factoring products\<close>

  definition factorizes :: "'a \<Rightarrow> bool" where
    "factorizes A \<equiv> (\<exists>B C. A \<simeq> B \<^bold>\<times> C)"
  
  declare factorizes_def[pdefs]
  
  lemma product_type_is_object: "factorizes A \<Longrightarrow> is_object A" 
    using factorizes_def[of A]
    by (metis existing_identity_def local.arrow_implies_is_object local.product_existence 
        local.product_of_types local.products_isomorphism)
       
subsection \<open>Induced arrows\<close>
  
  text \<open>This topic is more generic and could be formalized in a more general manner in
        Constructions.thy. The special case when a chosen product exists would then be instantiated 
        here.

        The more general version would define the function induced_by with an additional argument
        for the relevant product.\<close>

  definition induced_by :: "'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool" ("_ induced'_by _ _" [120, 120, 120] 120) where
    "m induced_by f g \<equiv> if dom f \<simeq> dom g 
                        then (\<pi>\<^sub>1 (cod f) (cod g)) \<cdot> m \<simeq> f 
                           \<and> (\<pi>\<^sub>2 (cod f) (cod g)) \<cdot> m \<simeq> g
                        else m = *"

  declare induced_by_def[pdefs]
  
  lemma unique_induced_by: "\<exists>!m. m induced_by f g"
  proof (cases "dom f \<simeq> dom g")
    case False
    show ?thesis using induced_by_def Let_def False by auto
  next
    case True
    have are_types: "is_object (cod f) \<and> is_object (cod g)"
      by (metis fl True local.composition_existence local.dom_implies_existence local.is_object_def 
          local.left_compose_id)
    hence "(cod f)\<leftarrow>f\<midarrow> (dom f) \<midarrow>g\<rightarrow>(cod g) \<longrightarrow> (\<exists>!u. (\<pi>\<^sub>1 (cod f) (cod g)) \<cdot> u \<simeq> f 
                                                   \<and> (\<pi>\<^sub>2 (cod f) (cod g)) \<cdot> u \<simeq> g)"
      using product_of_types[of "cod f" "cod g"] is_product_def by auto
    moreover have "(cod f) \<leftarrow>f\<midarrow> (dom f) \<midarrow>g\<rightarrow> (cod g)" using is_wedge_def True are_types
      by (simp add: fl local.arrow_def local.is_object_def) 
    ultimately show ?thesis by (simp add: induced_by_def True) 
  qed 
  
  definition bracket_operator :: "'a \<Rightarrow> 'a \<Rightarrow> 'a" ("\<langle>_,_\<rangle>") where
    "\<langle>f,g\<rangle> \<equiv> (THE m. m induced_by f g)"

  lemma induced_by_property: "\<langle>f,g\<rangle> induced_by f g"
    using theI'[of "\<lambda>m. m induced_by f g"] bracket_operator_def unique_induced_by by simp

  lemma induced_by_map: "dom f \<simeq> dom g \<Longrightarrow> \<langle>f,g\<rangle>:(dom f)\<rightarrow>(cod f)\<^bold>\<times>(cod g)" 
    by (smt induced_by_property induced_by_def local.arrow_def local.associativity 
        local.composition_existence local.is_object_def local.left_compose_id local.right_compose_id 
        projection1_map fl)

  lemma induced_by_and_monics: 
    assumes "is_monic f" and "is_monic g" and "dom f \<simeq> dom g"
      shows "is_monic (\<langle>f, g\<rangle>)"
    unfolding is_monic_def using assms unfolding is_monic_def using induced_by_property[of f g] 
    unfolding induced_by_def by (metis fl induced_by_map local.arrow_def local.associativity 
                                local.cod_implies_existence local.composition_existence)

subsection \<open>Products of arrows\<close>

  definition product_double_wedge_commutes 
    ("product'_double'_wedge'_commutes // \<box> \<leftarrow>\<pi>\<^sub>1\<midarrow>_\<^bold>\<times>_\<midarrow>\<pi>\<^sub>2\<rightarrow> \<box> // \<down>_\<down>_\<down>_ // \<box> \<leftarrow>\<pi>\<^sub>1\<midarrow>_\<^bold>\<times>_\<midarrow>\<pi>\<^sub>2\<rightarrow> \<box>") where
     "product_double_wedge_commutes \<box> \<leftarrow>\<pi>\<^sub>1\<midarrow> A\<^bold>\<times>B  \<midarrow>\<pi>\<^sub>2\<rightarrow> \<box>
                                    \<down>h        \<down>x       \<down>k
                                    \<box> \<leftarrow>\<pi>\<^sub>1\<midarrow> C\<^bold>\<times>D \<midarrow>\<pi>\<^sub>2\<rightarrow> \<box>
                              \<equiv> (h \<cdot> (\<pi>\<^sub>1 A B) \<simeq> (\<pi>\<^sub>1 C D) \<cdot> x \<and> k \<cdot> (\<pi>\<^sub>2 A B) \<simeq> (\<pi>\<^sub>2 C D) \<cdot> x)"

  thm product_double_wedge_commutes_def

  definition arrow_times :: "'a \<Rightarrow> 'a \<Rightarrow> 'a" (infix "\<^bold>\<times>\<^sub>a" 120) where
    "f \<^bold>\<times>\<^sub>a g \<equiv> \<langle>f \<cdot> (\<pi>\<^sub>1 (dom f) (dom g)), g \<cdot> (\<pi>\<^sub>2 (dom f) (dom g))\<rangle>"

  declare product_double_wedge_commutes_def[defs]

  lemma product_of_arrows: 
    assumes "E f" and "E g"
    shows "(f \<^bold>\<times>\<^sub>a g):(dom f)\<^bold>\<times>(dom g)\<rightarrow>(cod f)\<^bold>\<times>(cod g)" and
          "product_double_wedge_commutes \<box> \<leftarrow>\<pi>\<^sub>1\<midarrow> (dom f) \<^bold>\<times> (dom g) \<midarrow>\<pi>\<^sub>2\<rightarrow> \<box>
                                         \<down>f               \<down>(f \<^bold>\<times>\<^sub>a g)       \<down>g
                                         \<box> \<leftarrow>\<pi>\<^sub>1\<midarrow> (cod f) \<^bold>\<times> (cod g) \<midarrow>\<pi>\<^sub>2\<rightarrow> \<box>"
  proof -
    have types: "is_object (dom f) \<and> is_object (dom g) \<and> is_object (cod f) \<and> is_object (cod g)"
      unfolding is_object_def by (metis fl assms local.composition_existence local.left_compose_id 
      local.right_compose_id)

    have 1: "dom (f \<cdot> (\<pi>\<^sub>1 (dom f) (dom g))) \<simeq> (dom f) \<^bold>\<times> (dom g)"
      using assms types projection1_map
      by (smt fl local.arrow_def local.associativity local.cod_implies_existence 
          local.composition_existence local.right_compose_id)
    have 2: "dom (g \<cdot> (\<pi>\<^sub>2 (dom f) (dom g))) \<simeq> (dom f) \<^bold>\<times> (dom g)"
      using assms types projection2_map
      by (smt fl local.arrow_def local.associativity local.cod_implies_existence 
          local.composition_existence local.right_compose_id)
    have 3: "cod (f \<cdot> (\<pi>\<^sub>1 (dom f) (dom g))) = cod f" 
      by (metis fl \<open>dom f \<cdot> (\<pi>\<^sub>1 (dom f) (dom g)) \<simeq> (dom f) \<^bold>\<times> (dom g)\<close> local.associativity 
          local.composition_existence local.dom_implies_existence local.left_compose_id)
    have 4: "cod (g \<cdot> (\<pi>\<^sub>2 (dom f) (dom g))) = cod g" 
      by (metis fl \<open>dom g \<cdot> (\<pi>\<^sub>2 (dom f) (dom g)) \<simeq> (dom f) \<^bold>\<times> (dom g)\<close> local.associativity 
          local.composition_existence local.dom_implies_existence local.left_compose_id)
  
    show map: "(f \<^bold>\<times>\<^sub>a g):(dom f)\<^bold>\<times>(dom g)\<rightarrow>(cod f)\<^bold>\<times>(cod g)"
      using fl "1" "2" "3" "4" arrow_times_def induced_by_property induced_by_def local.arrow_def 
            local.associativity local.composition_existence local.product_of_types 
            local.right_compose_id projection2_map types 
      by (smt induced_by_map)

    show "product_double_wedge_commutes \<box> \<leftarrow>\<pi>\<^sub>1\<midarrow> (dom f) \<^bold>\<times> (dom g) \<midarrow>\<pi>\<^sub>2\<rightarrow> \<box>
                                        \<down>f               \<down>(f \<^bold>\<times>\<^sub>a g)       \<down>g
                                        \<box> \<leftarrow>\<pi>\<^sub>1\<midarrow> (cod f) \<^bold>\<times> (cod g) \<midarrow>\<pi>\<^sub>2\<rightarrow> \<box>" using map
    unfolding product_double_wedge_commutes_def arrow_times_def  
    using induced_by_property[of "f \<cdot> (\<pi>\<^sub>1 (dom f) (dom g))" "g \<cdot> (\<pi>\<^sub>2 (dom f) (dom g))"] 
    unfolding induced_by_def using 1 2 3 4 fl by simp
  qed
  
  lemma arrow_product_existence_1: "E (f \<^bold>\<times>\<^sub>a g) \<longrightarrow> E f \<and> E g" 
    by (metis arrow_times_def induced_by_property induced_by_def local.composition_existence 
        local.inexistent_element existing_identity_def)

  lemma arrow_product_existence_2: "E f \<and> E g \<longrightarrow> E (f \<^bold>\<times>\<^sub>a g)"
    using local.cod_implies_existence local.composition_existence product_double_wedge_commutes_def 
    product_of_arrows fl by meson
  
  lemma arrow_product_existence: "E (f \<^bold>\<times>\<^sub>a g) \<longleftrightarrow> E f \<and> E g" 
    using arrow_product_existence_1 arrow_product_existence_2 fl by metis

  lemma composition_and_arrow_product:
    assumes "cod f \<simeq> dom h" and "cod g \<simeq> dom i"
    shows "(h \<^bold>\<times>\<^sub>a i) \<cdot> (f \<^bold>\<times>\<^sub>a g) = (h \<cdot> f) \<^bold>\<times>\<^sub>a (i \<cdot> g)"
  proof - 
    have ex: "E f \<and> E g \<and> E h \<and> E i" 
      using assms(1) assms(2) local.cod_implies_existence local.dom_implies_existence fl by blast

    have comp1: "(h \<cdot> f) \<cdot> (\<pi>\<^sub>1 (dom f) (dom g)) \<simeq> (\<pi>\<^sub>1 (cod h) (cod i)) \<cdot> ((h \<cdot> f) \<^bold>\<times>\<^sub>a (i \<cdot> g))"
      using assms(1) product_of_arrows ex 
      unfolding product_double_wedge_commutes_def 
      by (smt assms(2) local.associativity local.cod_implies_existence local.composition_existence 
          local.left_compose_id fl)

    hence 1: "(\<pi>\<^sub>1 (cod h) (cod i)) \<cdot> ((h \<^bold>\<times>\<^sub>a i) \<cdot> (f \<^bold>\<times>\<^sub>a g)) 
            = (\<pi>\<^sub>1 (cod h) (cod i)) \<cdot> ((h \<cdot> f) \<^bold>\<times>\<^sub>a (i \<cdot> g))"
      using product_of_arrows unfolding product_double_wedge_commutes_def
      by (smt assms ex local.associativity fl)
      
    have comp2: "(i \<cdot> g) \<cdot> (\<pi>\<^sub>2 (dom f) (dom g)) \<simeq> (\<pi>\<^sub>2 (cod h) (cod i)) \<cdot> ((h \<cdot> f) \<^bold>\<times>\<^sub>a (i \<cdot> g))"
      using product_of_arrows ex unfolding product_double_wedge_commutes_def 
      by (smt assms local.associativity local.cod_implies_existence local.composition_existence 
          local.left_compose_id fl)

    hence 2: "(\<pi>\<^sub>2 (cod h) (cod i)) \<cdot> ((h \<^bold>\<times>\<^sub>a i) \<cdot> (f \<^bold>\<times>\<^sub>a g)) 
            = (\<pi>\<^sub>2 (cod h) (cod i)) \<cdot> ((h \<cdot> f) \<^bold>\<times>\<^sub>a (i \<cdot> g))"
      using product_of_arrows unfolding product_double_wedge_commutes_def
      by (smt assms(1) assms(2) associativity ex existing_identity_def kleene_equality_def)

    show ?thesis using 1 2 comp1 comp2
      using local.composition_existence local.dom_implies_existence local.product_existence 
            local.product_of_types local.projection1_existence fl 
      by (smt unique_arrow_into_product)
  qed


  lemma "A \<^bold>\<times> B \<simeq> C \<^bold>\<times> D \<Longrightarrow> A = C"
    nitpick oops (* this is no longer true rendering the following two impossible *)

  lemma dom_and_arrow_product:
    assumes "dom (f \<^bold>\<times>\<^sub>a g) \<simeq> A \<^bold>\<times> B" 
    shows "dom f = A" and "dom g = B"
    using assms nitpick oops (* this is no longer true *)

(*     subgoal by (smt arrow_product_existence assms local.arrow_def local.dom_implies_existence 
                local.product_existence product_of_arrows(1) projection1_map existing_identity_def)
    subgoal by (smt arrow_product_existence assms local.arrow_def local.dom_implies_existence 
                local.product_existence product_of_arrows(1) projection2_map existing_identity_def)
    done
 *)

  lemma cod_and_arrow_product:
    assumes "cod (f \<^bold>\<times>\<^sub>a g) \<simeq> A \<^bold>\<times> B" 
    shows "cod f = A" and "cod g = B"
    oops
(*     by (metis arrow_product_existence assms local.arrow_def local.cod_implies_existence 
        product_of_arrows(1) products_equal_factors_equal existing_identity_def)+ *)


end

end
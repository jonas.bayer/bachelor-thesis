theory Cartesian_Closed_Category imports "../Constructions/Exponentials" Cartesian_Category 

begin

section \<open>Cartesian Closed Categories\<close>

locale cartesian_closed_category = cartesian_category + 
  fixes exponential :: "'a \<Rightarrow> 'a \<Rightarrow> 'a exponential" (infixr "\<^bold>^" 80)

assumes 
        exponential: "is_object A \<and> is_object B \<Longrightarrow> (A \<^bold>^ B) exponential_of B by A" 
begin
  
  lemma "True" nitpick[satisfy] oops
  
end

end
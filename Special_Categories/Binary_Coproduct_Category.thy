theory Binary_Coproduct_Category imports "../Constructions/Coproducts" 
                                         "../Constructions/Monics_Epics"

begin

subsection \<open>Categories with binary coproducts\<close>

locale binary_coproduct_category = category +
    fixes coproduct :: "'a \<Rightarrow> 'a \<Rightarrow> 'a" (infix "\<squnion>" 120) and
          insertion1 :: "'a \<Rightarrow> 'a \<Rightarrow> 'a" ("\<iota>\<^sub>1 _ _") and
          insertion2 :: "'a \<Rightarrow> 'a \<Rightarrow> 'a" ("\<iota>\<^sub>2 _ _") 
  assumes 
          product_of_types[cat]:      "is_object A \<and> is_object B \<longrightarrow> 
                                       is_coproduct A \<midarrow>(\<iota>\<^sub>1 A B)\<rightarrow> (A \<squnion> B) \<leftarrow>(\<iota>\<^sub>2 A B)\<midarrow> B" and  
        
          product_existence[cat]:     "E (A \<squnion> B) \<longrightarrow> (is_object A \<and> is_object B)" and
          projection1_existence[cat]: "E (\<iota>\<^sub>1 A B) \<longrightarrow> (is_object A \<and> is_object B)" and
          projection2_existence[cat]: "E (\<iota>\<^sub>2 A B) \<longrightarrow> (is_object A \<and> is_object B)"

end
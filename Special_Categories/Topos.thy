theory Topos imports Cartesian_Closed_Category 
                     "../Constructions/Pullbacks"
begin

section \<open>Topoi\<close>

locale topos = cartesian_closed_category +
  fixes subobjectClassifier :: "'a" ("\<Omega>") and
        true :: "'a" ("t") 
  assumes 
        trueMap: "t:\<one>\<rightarrow>\<Omega>" and
        toposProperty: "is_monic s \<longrightarrow> (\<exists>!\<chi>. \<chi>:dom s\<rightarrow>\<Omega> \<and> is_pullback_diagram \<box> \<midarrow>!\<^sub>1 (dom s)\<rightarrow> \<box> 
                                                                              s\<down>               \<down>t 
                                                                              \<box>     \<midarrow> \<chi>\<rightarrow>     \<box> )" 
begin

subsection \<open>The classifying arrow\<close>

  definition classifyingArrow :: "'a \<Rightarrow> 'a" ("\<chi>") where
    "\<chi> s \<equiv> (THE x. (is_monic s \<longrightarrow> x:dom s\<rightarrow>\<Omega> \<and> is_pullback_diagram \<box> \<midarrow>!\<^sub>1 (dom s)\<rightarrow> \<box> 
                                                                    s\<down>               \<down>t 
                                                                     \<box>     \<midarrow> x\<rightarrow>    \<box>  ) 
                 \<and> (\<not> is_monic s \<longrightarrow> x = *))"
  
  lemma toposPropertyLemma:
    assumes "is_monic s"
    shows "(\<chi> s):dom s\<rightarrow>\<Omega>" and "is_pullback_diagram \<box> \<midarrow>!\<^sub>1 (dom s)\<rightarrow> \<box> 
                                                   s\<down>               \<down>t 
                                                    \<box>    \<midarrow>\<chi> s\<rightarrow>     \<box> "
  proof - 
    presume "is_monic s"
    hence "\<exists>!x. x:dom s\<rightarrow>\<Omega> \<and> is_pullback_diagram \<box> \<midarrow>!\<^sub>1 (dom s)\<rightarrow> \<box> 
                                                s\<down>               \<down>t 
                                                 \<box>     \<midarrow> x\<rightarrow>     \<box>  " using toposProperty by auto
    moreover have "\<chi> s = (THE x::'a. x:dom s\<rightarrow>\<Omega> \<and> is_pullback_diagram \<box> \<midarrow>!\<^sub>1 (dom s)\<rightarrow> \<box> 
                                                                      s\<down>               \<down>t 
                                                                      \<box>     \<midarrow> x\<rightarrow>     \<box> )" 
      using classifyingArrow_def[of s] \<open>is_monic s\<close> by auto

    ultimately show "(\<chi> s):dom s\<rightarrow>\<Omega>" and "is_pullback_diagram \<box> \<midarrow>!\<^sub>1 (dom s)\<rightarrow> \<box> 
                                                              s\<down>               \<down>t 
                                                              \<box>    \<midarrow>\<chi> s\<rightarrow>     \<box> "
      using theI'[of "\<lambda>x. x:dom s\<rightarrow>\<Omega> \<and> is_pullback_diagram \<box> \<midarrow>!\<^sub>1 (dom s)\<rightarrow> \<box> 
                                                           s\<down>               \<down>t 
                                                            \<box>     \<midarrow> x\<rightarrow>     \<box> "] 
      by smt+
  qed (fact assms)+


subsection \<open>Porperties of Monics and Epics in Topoi\<close>
  
  lemma monicsAreEqualizers: "is_monic s \<Longrightarrow> s equalizer_of (\<chi> s) (t \<cdot> !\<^sub>1 (cod s))"
  proof - 
    assume "is_monic s"
    hence characteristicMap: "\<chi> s:dom s\<rightarrow>\<Omega> \<and> is_pullback_diagram \<box> \<midarrow>!\<^sub>1 (dom s)\<rightarrow> \<box>
                                                                 s\<down>               \<down>t 
                                                                 \<box>    \<midarrow> \<chi> s \<rightarrow>   \<box>"
      using toposPropertyLemma by auto
    
    have parallelPair: "is_parallel_pair (\<chi> s) (t \<cdot> !\<^sub>1 (cod s))"
      unfolding is_parallel_pair_def
      by (smt characteristicMap local.arrow_def local.associativity local.composition_existence 
          local.is_pullback_def local.left_compose_id local.right_compose_id fl)
  
    have equalizes: "s equalizes (\<chi> s) (t \<cdot> !\<^sub>1 (cod s))"
      using final_map_composition[of s] \<open>is_monic s\<close> unfolding equalizes_def is_monic_def 
      using characteristicMap unfolding is_pullback_def using associativity fl by smt 
  
    have "h equalizes (\<chi> s) (t \<cdot> !\<^sub>1 (cod s)) \<longrightarrow> h factors_uniquely_through s" for h 
    proof (rule impI) 
      assume "h equalizes (\<chi> s) (t \<cdot> !\<^sub>1 (cod s))"
  
      hence "t \<cdot> !\<^sub>1 (dom h) \<simeq> \<chi> s \<cdot> h" unfolding equalizes_def 
        by (metis equalizes local.associativity local.cod_implies_existence 
            local.composition_existence local.equalizes_def local.final_map_composition fl)
  
      thus "h factors_uniquely_through s" 
        unfolding factors_uniquely_through_def using characteristicMap 
        unfolding is_pullback_def by (metis \<open>is_monic s\<close> existing_identity_def local.is_monic_def)
    qed
  
    thus ?thesis
    unfolding equalizer_of_def using parallelPair equalizes by auto
  qed
  
  lemma "is_monic m \<Longrightarrow> is_epic m \<Longrightarrow> is_isomorphism m"
    using monicsAreEqualizers epic_equalizer_is_isomorphism by blast

subsection \<open>t is monic\<close>

  lemma tIsMonic: "is_monic t"
    by (smt local.arrow_implies_is_object local.arrow_def local.associativity composition_existence 
        local.final_map local.final_map_composition local.final_object local.is_final_def 
        local.is_monic_def local.left_compose_id local.product_final local.products_isomorphism 
        local.trueMap fl)

end

end
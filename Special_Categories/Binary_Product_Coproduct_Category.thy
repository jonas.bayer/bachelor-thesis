theory Binary_Product_Coproduct_Category imports Binary_Product_Category
                                                 Binary_Coproduct_Category
                                                 "../Constructions/Isomorphisms"

begin

subsection \<open>Categories with binary products and coproducts\<close>

  locale binary_product_coproduct_category = binary_product_category 
                                           + binary_coproduct_category
  begin

  text \<open>If a category has products and coproducts one can investigate if it is distributive. To 
        state the right notion of distributivity it is important to define the right kind of 
        equality, suggestively named kleene_isomorphic here: \<close>

  definition kleene_isomorphic :: "'a \<Rightarrow> 'a \<Rightarrow> bool" (infix "\<approx>" 56) where
    "x \<approx> y \<equiv> E x \<or> E y \<longrightarrow> x isomorphic_to y"
 
  declare kleene_isomorphic_def[fl]

  text \<open>The following definition states what it means for a category to be distributive: \<close>

  definition distributive_category :: "bool" where
    "distributive_category \<equiv> \<forall>A B C. (A \<^bold>\<times> (B \<squnion> C)) \<approx> ((A \<^bold>\<times> B) \<squnion> (A \<^bold>\<times> C))"


  text \<open>One can prove the following auxiliary lemma which will be helpful in later proofs. \<close>

  lemma distributivity_existence:
    "E (A \<^bold>\<times> (B \<squnion> C)) \<or> E ((A \<^bold>\<times> B) \<squnion> (A \<^bold>\<times> C)) \<longleftrightarrow> is_object A \<and> is_object B \<and> is_object C"
    by (metis arrow_implies_is_object existing_identity_def is_coproduct_def is_cowedge_def 
        is_object_def product_existence product_exists_implies_is_object product_is_object 
        product_of_types)

  text \<open>In order to make model finding generate more easily comprehensible models it is useful to
        additionally assert that there only is one single inexistent element. \<close>

  context 
    assumes "\<exists>!x. \<not> E x"
  begin

  text \<open>Then one can let Isabelle check for counterexamples: \<close>

  lemma distributive_law: "(A \<^bold>\<times> (B \<squnion> C)) \<approx> ((A \<^bold>\<times> B) \<squnion> (A \<^bold>\<times> C))"
    unfolding kleene_isomorphic_def distributivity_existence
    nitpick[card = 14, box = false, timeout = 1] oops

  lemma distributive_law_equality: 
    assumes "is_object A \<and> is_object B \<and> is_object C"
    shows "A \<^bold>\<times> (B \<squnion> C) = (A \<^bold>\<times> B) \<squnion> (A \<^bold>\<times> C)"
    nitpick[ box=false, timeout = 1] oops

  text \<open>This is harder for Isabelle than finding an example of a non-distributive lattice. I think
        the main reason for this is that the smallest non-distributive lattice has 5 elements but
        the corresponding category is already bigger with 13 arrows in total (each lattice element
        yiels an identity + the arrows between different elements). Thus, the models are a bit 
        larger and finding them takes longer. I set the timeout to 5 minutes, and hinted the tool 
        at the right number of arrows, but still Isabelle was not able to find a model.  \<close>

  end 



end

end
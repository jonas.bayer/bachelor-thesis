theory Exponentials imports "../Special_Categories/Binary_Product_Category"

begin

subsection \<open>Exponentials\<close>

record 'a exponential = 
  object :: 'a
  evaluation :: 'a

context binary_product_category 
begin

subsubsection \<open>Definition and simple properties\<close>
  
  definition exponential_of :: "'a exponential \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool" 
    ("_ exponential'_of _ by _" [120, 120, 120] 120) where
    "e exponential_of B by A \<equiv> (evaluation e):(object e)\<^bold>\<times>A\<rightarrow>B 
                             \<and> (\<forall>g C. g:C\<^bold>\<times>A\<rightarrow>B \<longrightarrow> (\<exists>!g'. g \<simeq> (evaluation e) \<cdot> (g' \<^bold>\<times>\<^sub>a A)))"
  
  lemma exp_implies_types: 
    assumes "e exponential_of B by A"
      shows "is_object A" and "is_object B" and "is_object (object e)" 
    using assms unfolding exponential_of_def 
    using arrow_implies_is_object product_is_object apply blast
    using arrow_implies_is_object assms exponential_of_def apply blast
    using arrow_implies_is_object assms exponential_of_def product_is_object by blast
      
  lemma dom_cod_of_exponential:
    assumes "e exponential_of B by A"
    shows "dom (evaluation e) \<simeq> (object e)\<^bold>\<times>A" and "cod (evaluation e) \<simeq> B" 
    using assms unfolding exponential_of_def arrow_def by blast+

end

end
theory Coproducts imports Isomorphisms

begin

subsection \<open>Coproducts\<close>

context category
begin

  definition is_cowedge ("_\<midarrow>_\<rightarrow>_\<leftarrow>_\<midarrow>_" [120,120,120,120,120] 120) where
    "A \<midarrow>f\<rightarrow> C \<leftarrow>g\<midarrow> B \<equiv> f:A\<rightarrow>C \<and> g:B\<rightarrow>C"

  definition is_coproduct :: "'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool" 
    ("is'_coproduct _ \<midarrow>_\<rightarrow> _ \<leftarrow>_\<midarrow> _" [120,120,120,120,120] 120) where
    "is_coproduct A \<midarrow>i1\<rightarrow> P \<leftarrow>i2\<midarrow> B \<equiv> A \<midarrow>i1\<rightarrow> P \<leftarrow>i2\<midarrow> B \<and> 
                           (\<forall>h T k. A \<midarrow>h\<rightarrow> T \<leftarrow>k\<midarrow> B \<longrightarrow> (\<exists>!u. u\<cdot>i1 \<simeq> h \<and> u\<cdot>i2 \<simeq> k))"

end

end
theory Isomorphisms imports "../Category"

begin

subsection \<open>Isomorphisms\<close>

context category 
begin

  definition inverse_of :: "'a \<Rightarrow> 'a \<Rightarrow> bool" (infix "inverse'_of" 120) where
    "f inverse_of g \<equiv> is_object (f\<cdot>g) \<and> is_object (g\<cdot>f)"

  definition is_isomorphism :: "'a \<Rightarrow> bool" where
    "is_isomorphism f \<equiv> \<exists>g. g inverse_of f"

  definition isomorphism_arrow ("_:_\<Rightarrow>_" [120,120,120] 119) where
    "f:A\<Rightarrow>B \<equiv> f:A\<rightarrow>B \<and> is_isomorphism f"

  definition isomorphic_to :: "'a \<Rightarrow> 'a \<Rightarrow> bool" (infix "isomorphic'_to" 120) where
    "A isomorphic_to B \<equiv> \<exists>f. f:A\<Rightarrow>B"

  declare inverse_of_def[defs] is_isomorphism_def[defs] isomorphism_arrow_def[defs] 
          isomorphic_to_def[defs]

  lemma isomorphism_has_unique_inverse[cat]: "is_isomorphism f \<Longrightarrow> \<exists>!g. g inverse_of f"
    unfolding defs using cat fl by smt

  lemma isomorphism_inverse[cat]: 
    "is_isomorphism f \<Longrightarrow> g inverse_of f \<Longrightarrow> g:(cod f)\<rightarrow>(dom f) \<and> is_isomorphism g"
    unfolding defs using cat fl by smt

  lemma isomorphic_to_symmetric[cat]: "A isomorphic_to B \<longleftrightarrow> B isomorphic_to A" 
    unfolding defs using cat fl by smt

end

end
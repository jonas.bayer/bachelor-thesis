theory Pullbacks imports Monics_Epics Products Equalizers "../Utilities/Square_Diagrams"

begin

subsection \<open>Pullbacks\<close> 

context category 
begin

  definition is_corner :: "'a \<Rightarrow> 'a \<Rightarrow> bool" where
    "is_corner f g = cod f \<simeq> cod g"

  definition is_pullback :: "'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool" where
   "is_pullback f g p1 p2 \<equiv> (f \<cdot> p1 \<simeq> g \<cdot> p2 \<and> (\<forall>h k. f\<cdot>h \<simeq> g\<cdot>k \<longrightarrow> (\<exists>!u. p1 \<cdot> u \<simeq> h \<and> p2 \<cdot> u \<simeq> k)))"

  text \<open>cf. the Picture on page 41 of McLarty's book \<close>
  abbreviation is_pullback_diagram :: "'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool"
  ("is'_pullback'_diagram // \<box> \<midarrow>_\<rightarrow> \<box> // _\<down>  \<down>_ // \<box> \<midarrow>_\<rightarrow> \<box>" [120, 120, 120, 120] 120) where
   "is_pullback_diagram \<box> \<midarrow>p2\<rightarrow> \<box> 
                      p1\<down>        \<down>g 
                        \<box> \<midarrow> f\<rightarrow> \<box>
    \<equiv> is_pullback f g p1 p2"

  declare is_corner_def[defs] is_pullback_def[defs]

  lemma equalizer_of_product_is_pullback:
    assumes "is_product (dom f) \<leftarrow>p1\<midarrow> P \<midarrow>p2\<rightarrow> (dom g)"
        and "e equalizer_of (f\<cdot>p1) (g\<cdot>p2)"
      shows "is_pullback_diagram   \<box> \<midarrow>p2\<cdot>e\<rightarrow>\<box> 
                             p1\<cdot>e\<down>        \<down>g 
                                 \<box> \<midarrow> f \<rightarrow>\<box>"
  proof - 
      have "f \<cdot> (p1 \<cdot> e) \<simeq> g \<cdot> (p2 \<cdot> e)" 
        using assms(2) equalizer_of_def equalizes_def associativity fl by smt
  
      moreover have "f \<cdot> h \<simeq> g \<cdot> k \<longrightarrow> (\<exists>!u. (p1 \<cdot> e) \<cdot> u \<simeq> h \<and> (p2 \<cdot> e) \<cdot> u \<simeq> k)" for h k
      proof (rule impI)
        assume "f \<cdot> h \<simeq> g \<cdot> k" 

        hence "(dom f)\<leftarrow>h\<midarrow> (dom h) \<midarrow>k\<rightarrow>(dom g)" unfolding is_wedge_def arrow_def using cat fl by smt

        hence "\<exists>!x. p1 \<cdot> x \<simeq> h \<and> p2 \<cdot> x \<simeq> k" 
          using assms(1) unfolding is_product_def by auto 

        then obtain x where x: "p1 \<cdot> x \<simeq> h \<and> p2 \<cdot> x \<simeq> k" by auto
    
        have "(f \<cdot> p1) \<cdot> x \<simeq> (g \<cdot> p2) \<cdot> x \<longrightarrow> (\<exists>!u. e \<cdot> u \<simeq> x)"
          using assms(2) unfolding equalizer_of_def equalizes_def factors_uniquely_through_def 
          by auto
    
        hence "\<exists>!u. e \<cdot> u \<simeq> x" 
          using associativity x \<open>f \<cdot> h \<simeq> g \<cdot> k\<close> fl by auto
        
        show "\<exists>!u. (p1 \<cdot> e) \<cdot> u \<simeq> h \<and> (p2 \<cdot> e) \<cdot> u \<simeq> k"
          by (smt \<open>\<exists>!u. e \<cdot> u \<simeq> x\<close> \<open>\<exists>!x. p1 \<cdot> x \<simeq> h \<and> p2 \<cdot> x \<simeq> k\<close> fl local.associativity x)
      qed
  
      ultimately show ?thesis unfolding is_pullback_def by auto
  qed

  lemma pullback_arrow_to_product_is_equalizer:
    assumes "is_product (dom f) \<leftarrow>p1\<midarrow> P \<midarrow>p2\<rightarrow> (dom g)"
        and "is_pullback_diagram   \<box> \<midarrow>b2\<rightarrow> \<box>
                               b1\<down>        \<down>g
                                 \<box> \<midarrow> f\<rightarrow> \<box>"
      shows "\<exists>e. e:(dom b1)\<rightarrow>P \<and> e equalizer_of (f\<cdot>p1) (g\<cdot>p2)"
  proof - 

    have "(dom f)\<leftarrow>b1\<midarrow> (dom b1) \<midarrow>b2\<rightarrow>dom g" unfolding is_wedge_def arrow_def
      by (smt assms(2) is_pullback_def local.composition_existence fl)
    
    hence "\<exists>!e. p1 \<cdot> e \<simeq> b1 \<and> p2 \<cdot> e \<simeq> b2"
      using assms(1) unfolding is_product_def by auto 

    then obtain e where e: "p1 \<cdot> e \<simeq> b1 \<and> p2 \<cdot> e \<simeq> b2" by auto
    
    have equalizerProperty: "(f \<cdot> p1) \<cdot> h \<simeq> (g \<cdot> p2) \<cdot> h \<longrightarrow> (\<exists>!u. e \<cdot> u \<simeq> h)" for h
    proof (rule impI) 
      assume "(f \<cdot> p1) \<cdot> h \<simeq> (g \<cdot> p2) \<cdot> h"

      hence "f \<cdot> (p1 \<cdot> h) \<simeq> g \<cdot> (p2 \<cdot> h)" using associativity fl by metis

      hence "\<exists>!u. b1 \<cdot> u \<simeq> p1 \<cdot> h \<and> b2 \<cdot> u \<simeq> p2 \<cdot> h" 
        using assms(2) unfolding is_pullback_def associativity by auto

      then obtain u where u: "b1 \<cdot> u \<simeq> p1 \<cdot> h \<and> b2 \<cdot> u \<simeq> p2 \<cdot> h" by auto

      hence "p1 \<cdot> (e \<cdot> u) \<simeq> p1 \<cdot> h \<and> p2 \<cdot> (e \<cdot> u) \<simeq> p2 \<cdot> h" using e associativity fl by auto

      hence "e \<cdot> u \<simeq> h" 
        using assms(1) unique_arrow_into_product by blast

      thus "\<exists>!u. e \<cdot> u \<simeq> h"
        by (metis \<open>\<exists>!u. b1 \<cdot> u \<simeq> p1 \<cdot> h \<and> b2 \<cdot> u \<simeq> p2 \<cdot> h\<close> e local.associativity fl)
    qed

    show ?thesis apply (rule exI[of _ e], auto) 
      subgoal by (metis assms(1) e arrow_def associativity cod_implies_existence composition_existence 
                  right_compose_id products_isomorphism fl) using assms(2)
      subgoal unfolding equalizer_of_def is_parallel_pair_def equalizes_def 
              factors_uniquely_through_def 
              using equalizerProperty is_pullback_def associativity composition_existence 
                    dom_implies_existence left_compose_id unfolding fl 
              by (smt existing_identity_def e)         
      done
  qed

  lemma arrows_to_pullback:
    assumes "is_pullback_diagram   \<box> \<midarrow>p2\<rightarrow> \<box>
                               p1\<down>        \<down>g
                                 \<box> \<midarrow> f \<rightarrow>\<box>"
      and "p1 \<cdot> r \<simeq> p1 \<cdot> s" and "p2 \<cdot> s \<simeq> p2 \<cdot> r"
    shows "r \<simeq> s"
  using assms unfolding is_pullback_def 
    by (metis local.associativity local.cod_implies_existence fl local.composition_existence)  

subsection \<open>Guises of pullbacks\<close>

  lemma pullback_of_monic_1:
    assumes "is_pullback_diagram   \<box> \<midarrow>p2\<rightarrow> \<box>
                               p1\<down>        \<down>g
                                 \<box> \<midarrow> f \<rightarrow>\<box>"
        and "is_monic g"
      shows "is_monic p1"
  proof - 
    have "p1 \<cdot> h \<simeq> p1 \<cdot> k \<longrightarrow> h \<simeq> k" for h k
    proof (rule impI) 
      assume "p1 \<cdot> h \<simeq> p1 \<cdot> k"
      hence "g \<cdot> (p2 \<cdot> h) \<simeq> g \<cdot> (p2 \<cdot> k)" 
        using assms(1) unfolding is_pullback_def by (smt associativity fl composition_existence)
      thus "h \<simeq> k" using assms unfolding is_monic_def is_pullback_def
        by (smt \<open>p1 \<cdot> h \<simeq> p1 \<cdot> k\<close> associativity cod_implies_existence fl composition_existence) 
    qed
  
    thus ?thesis unfolding is_monic_def using assms(1) is_pullback_def fl by smt
  qed

  lemma pullback_of_monic_2:
    assumes "is_pullback_diagram   \<box> \<midarrow>p2\<rightarrow> \<box>
                               p1\<down>        \<down>g
                                 \<box> \<midarrow> f \<rightarrow>\<box>"
        and "is_monic f"
      shows "is_monic p2"
  proof - 
    have "p2 \<cdot> h \<simeq> p2 \<cdot> k \<longrightarrow> h \<simeq> k" for h k
    proof (rule impI) 
      assume "p2 \<cdot> h \<simeq> p2 \<cdot> k"
      hence "f \<cdot> (p1 \<cdot> h) \<simeq> f \<cdot> (p1 \<cdot> k)" 
        using assms(1) unfolding is_pullback_def by (smt associativity fl composition_existence)
      thus "h \<simeq> k" using assms unfolding is_monic_def is_pullback_def
        by (smt \<open>p2 \<cdot> h \<simeq> p2 \<cdot> k\<close> associativity cod_implies_existence fl composition_existence) 
    qed
  
    thus ?thesis unfolding is_monic_def using assms(1) is_pullback_def by (smt fl)
  qed
 
  lemma pullback_inverse_image:
    assumes "is_pullback_diagram   \<box> \<midarrow>p2\<rightarrow> \<box>
                               i'\<down>        \<down>i
                                 \<box> \<midarrow> f \<rightarrow>\<box>"
        and "is_monic i" and "cod x \<simeq> dom f"
      shows "x member_of i' \<longleftrightarrow> (f \<cdot> x) member_of i"
      unfolding member_of_def generalized_element_def using assms unfolding is_pullback_def
      by (smt local.arrow_def local.associativity local.composition_existence 
          local.left_compose_id local.right_compose_id fl)
  
  lemma pullback_intersection:
    assumes "is_pullback_diagram   \<box> \<midarrow>p2\<rightarrow> \<box>
                               p1\<down>        \<down>j
                                 \<box> \<midarrow> i \<rightarrow>\<box>"
        and "is_monic i" and "is_monic j" defines "ij \<equiv> composition i p1"
      shows "x member_of ij \<longleftrightarrow> x member_of i \<and> x member_of j"
    using assms unfolding member_of_def ij_def is_pullback_def is_monic_def fl
    by (smt local.arrow_def local.associativity local.composition_existence local.left_compose_id 
        local.right_compose_id fl)

subsection \<open>Theorems on Pullbacks\<close>

  text \<open>cf. McLarty page 45 Theorem 4.8\<close>
  lemma pullback_double_square:
    assumes "double_square_commutes
              \<box> \<midarrow>k\<rightarrow> \<box> \<midarrow>m\<rightarrow> \<box>
             h\<down>      i\<down>       \<down>j
              \<box> \<midarrow>f\<rightarrow> \<box> \<midarrow>g\<rightarrow> \<box>"

        and "is_pullback_diagram \<box> \<midarrow> m\<rightarrow> \<box>
                                i\<down>        \<down>j
                                 \<box> \<midarrow> g \<rightarrow>\<box>"

      shows "is_pullback_diagram \<box> \<midarrow> k \<rightarrow> \<box>
                                h\<down>         \<down>i
                                 \<box> \<midarrow> f \<rightarrow> \<box> 

         \<longleftrightarrow> is_pullback_diagram \<box> \<midarrow>m \<cdot> k\<rightarrow> \<box>
                                h\<down>          \<down>j
                                 \<box> \<midarrow>g \<cdot> f\<rightarrow> \<box>"
  proof 
    assume left_pullback: "is_pullback_diagram \<box> \<midarrow> k \<rightarrow> \<box>
                                             h\<down>         \<down>i
                                              \<box> \<midarrow> f \<rightarrow> \<box>"

    hence "(g \<cdot> f) \<cdot> s \<simeq> j \<cdot> r \<Longrightarrow> (\<exists>!u. h \<cdot> u \<simeq> s \<and> (m \<cdot> k) \<cdot> u \<simeq> r)" for r s
      proof - 
       assume asm: "(g \<cdot> f) \<cdot> s \<simeq> j \<cdot> r"
       then obtain u where u: "r \<simeq> m \<cdot> u \<and> f \<cdot> s = i \<cdot> u"
        using assms(2) unfolding is_pullback_def 
        by (metis local.associativity fl)
       have "\<exists>!v. u \<simeq> k \<cdot> v \<and> s = h \<cdot> v"
        using left_pullback unfolding is_pullback_def
        by (metis asm local.associativity fl local.cod_implies_existence local.composition_existence u)
       thus ?thesis using u asm assms(1) unfolding squares 
         by (smt arrows_to_pullback assms(2) local.associativity local.cod_implies_existence 
            local.composition_existence fl)
      qed
    
    thus "is_pullback_diagram   \<box> \<midarrow>m \<cdot> k\<rightarrow> \<box>
                             h\<down>          \<down>j
                              \<box> \<midarrow>g \<cdot> f\<rightarrow> \<box>" 
      unfolding is_pullback_def using assms(1) unfolding squares using fl cat_ax by smt

  next

    assume outer_pullback: "is_pullback_diagram \<box> \<midarrow>m \<cdot> k\<rightarrow> \<box>
                                               h\<down>          \<down>j
                                                \<box> \<midarrow>g \<cdot> f\<rightarrow> \<box>"

    have "f \<cdot> t \<simeq> i \<cdot> q \<Longrightarrow> (\<exists>!u. h \<cdot> u \<simeq> t \<and> k \<cdot> u \<simeq> q)" for q t 
    proof - 
      assume "f \<cdot> t \<simeq> i \<cdot> q"
      then obtain u where u: "t \<simeq> h \<cdot> u \<and> m \<cdot> q \<simeq> (m \<cdot> k) \<cdot> u"
        using outer_pullback unfolding is_pullback_def fl
              assms(1) local.associativity local.composition_existence 
        by (smt assms(1) associativity category_axioms category_def cod_implies_existence 
            free_logic.existing_identity_def free_logic.kleene_equality_def rd_square_commutes_def)
      thus ?thesis
        by (smt \<open>f \<cdot> t \<simeq> i \<cdot> q\<close> arrows_to_pullback assms(1) assms(2) rd_square_commutes_def 
            local.associativity outer_pullback fl)
    qed
    
    thus "is_pullback_diagram   \<box> \<midarrow> k \<rightarrow> \<box>
                             h\<down>         \<down>i
                              \<box> \<midarrow> f \<rightarrow> \<box>"
      unfolding is_pullback_def using assms(1) unfolding squares using cat_ax fl by smt
  qed

  text \<open>Note that this last proof actually needs less symbols than the proof in McLarty e.g. 
        McLarty introduces w in the ==> direction.\<close>

end

end
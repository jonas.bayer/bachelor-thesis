theory Finals_Initials imports Isomorphisms

begin

subsection \<open>Final and Initial objects\<close>

context category
begin

  definition is_final :: "'a \<Rightarrow> bool" where
    "is_final P \<equiv> is_object P \<and> (\<forall>A. (is_object A) \<longrightarrow> (\<exists>!f. f:A\<rightarrow>P))"

  definition is_initial :: "'a \<Rightarrow> bool" where
    "is_initial I \<equiv> is_object I \<and> (\<forall>A. (is_object A) \<longrightarrow> (\<exists>!f. f:I\<rightarrow>A))"

  declare is_final_def[defs] is_initial_def[defs]

  lemma isomorphic_to_final_is_final[cat]: "is_final P \<Longrightarrow> f:Q\<Rightarrow>P \<Longrightarrow> is_final Q"
    unfolding defs using cat fl by smt

  lemma final_are_isomorphic[cat]: "is_final P \<Longrightarrow> is_final Q \<Longrightarrow> (\<exists>f. f:P\<Rightarrow>Q)" 
    unfolding defs by (smt associativity composition_existence left_compose_id right_compose_id fl)

  lemma isomorphic_to_initial_is_initial[cat]: "is_initial A \<and> f:A\<Rightarrow>B \<longrightarrow> is_initial B" 
    unfolding defs using cat fl by smt

  lemma initials_are_isomorphic[cat]: "is_initial A \<and> is_initial B \<longrightarrow> A isomorphic_to B"
    unfolding defs fl using cat 
    by (smt existing_identity_def kleene_equality_def)      

end

end
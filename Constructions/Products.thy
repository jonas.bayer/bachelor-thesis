theory Products imports "../Category" Isomorphisms

begin

subsection \<open>Products\<close>

named_theorems pdefs

context category 
begin

  definition is_wedge ("_\<leftarrow>_\<midarrow> _ \<midarrow>_\<rightarrow>_" [120,120,120,120,120] 120) where
    "A \<leftarrow>f\<midarrow> C \<midarrow>g\<rightarrow> B \<equiv> f:C\<rightarrow>A \<and> g:C\<rightarrow>B"

  definition is_product :: "'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool" 
    ("is'_product _\<leftarrow>_\<midarrow> _ \<midarrow>_\<rightarrow>_" [120,120,120,120,120] 120) where
    "is_product A \<leftarrow>p1\<midarrow> P \<midarrow>p2\<rightarrow> B \<equiv> A \<leftarrow>p1\<midarrow> P \<midarrow>p2\<rightarrow> B \<and> 
                           (\<forall>h T k. A \<leftarrow>h\<midarrow> T \<midarrow>k\<rightarrow> B \<longrightarrow> (\<exists>!u. p1\<cdot>u \<simeq> h \<and> p2\<cdot>u \<simeq> k))"

  declare is_wedge_def[pdefs] is_product_def[pdefs]

  text \<open>Some simple lemmas on products: \<close>
  lemma isomorphic_to_product_is_product:
    assumes "is_product A \<leftarrow>p1\<midarrow> P \<midarrow>p2\<rightarrow> B"
        and "f:Q\<rightarrow>P" and "is_isomorphism f"
      shows "is_product A \<leftarrow>p1 \<cdot> f\<midarrow> Q \<midarrow>p2 \<cdot> f\<rightarrow> B"
    using assms unfolding defs pdefs apply simp unfolding fl by (smt cat fl)
  
  lemma products_isomorphism:
    assumes "is_product A \<leftarrow>p1\<midarrow> P \<midarrow>p2\<rightarrow> B"
        and "is_product A \<leftarrow>q1\<midarrow> Q \<midarrow>q2\<rightarrow> B"
      shows "\<exists>u. u:Q\<rightarrow>P \<and> p1\<cdot>u\<simeq>q1 \<and> p2\<cdot>u\<simeq>q2 \<and> is_isomorphism u"
    using assms unfolding defs pdefs apply simp by (smt cat fl)

  lemma products_isomorphic:
    assumes "is_product A \<leftarrow>p1\<midarrow> P \<midarrow>p2\<rightarrow> B"
        and "is_product A \<leftarrow>q1\<midarrow> Q \<midarrow>q2\<rightarrow> B"
      shows "Q isomorphic_to P"
    using assms unfolding defs pdefs apply simp by (smt cat fl)

  lemma unique_arrow_into_product:
    assumes "is_product A \<leftarrow>p1\<midarrow> P \<midarrow>p2\<rightarrow> B"
        and "p1 \<cdot> f \<simeq> p1 \<cdot> g" and "p2 \<cdot> f \<simeq> p2 \<cdot> g" 
      shows "f \<simeq> g"
    using assms unfolding is_product_def is_wedge_def arrow_def
    using associativity cod_implies_existence composition_existence left_compose_id 
          right_compose_id unfolding fl 
    by smt

end

end
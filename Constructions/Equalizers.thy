theory Equalizers imports Monics_Epics Subobjects

begin

subsection \<open>Equalizers\<close>

context category 
begin

  definition is_parallel_pair :: "'a \<Rightarrow> 'a \<Rightarrow> bool" where
    "is_parallel_pair f g \<equiv> dom f \<simeq> dom g \<and> cod f \<simeq> cod g"

  definition equalizes :: "'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool" ("_ equalizes _ _" [120, 120, 120]) where
    "h equalizes f g \<equiv> f\<cdot>h \<simeq> g\<cdot>h"

  definition factors_uniquely_through :: "'a \<Rightarrow> 'a \<Rightarrow> bool" (infix "factors'_uniquely'_through" 120) 
    where
    "h factors_uniquely_through e \<equiv> (\<exists>!u. e\<cdot>u \<simeq> h)"

  definition equalizer_of :: "'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool" 
    ("_ equalizer'_of _ _" [120, 120, 120] 120) where 
    "(e equalizer_of f g) \<equiv> is_parallel_pair f g 
                         \<and> e equalizes f g 
                         \<and> (\<forall>h. h equalizes f g \<longrightarrow> h factors_uniquely_through e)"

  declare is_parallel_pair_def[defs] equalizes_def[defs] factors_uniquely_through_def[defs] 
          equalizer_of_def[defs]

  lemma equalizers_are_monic[cat]: "e equalizer_of f g \<Longrightarrow> is_monic e"
    unfolding defs fl by (metis associativity fl right_compose_id composition_existence)
 
  lemma epic_equalizer_is_isomorphism[cat]:
    assumes "e equalizer_of f g" and "is_epic e"
    shows "is_isomorphism e" 
    using assms unfolding defs using cat fl by smt

  lemma equalizers_and_subobjects[cat]: 
    assumes "is_parallel_pair f g" and "e is_subobject_of (dom f)"
    shows "e equalizer_of f g \<longleftrightarrow> (\<forall>x. cod x \<simeq> dom f \<longrightarrow> (x member_of e \<longleftrightarrow> f\<cdot>x \<simeq> g\<cdot>x))" 
    using assms unfolding defs fl using cat fl by smt

end

end
theory Monics_Epics imports Isomorphisms

begin

subsection \<open>Monics and Epis\<close>

context category
begin

  definition is_monic :: "'a \<Rightarrow> bool" where
    "is_monic m \<equiv> E m \<and> (\<forall>f g. m\<cdot>f \<simeq> m\<cdot>g \<longrightarrow> f \<simeq> g)"

  definition is_epic :: "'a \<Rightarrow> bool" where
    "is_epic m \<equiv> E m \<and> (\<forall>f g. f\<cdot>m \<simeq> g\<cdot>m \<longrightarrow> f \<simeq> g)"

  declare is_monic_def[defs] is_epic_def[defs] 

  lemma isomorphisms_monic[cat]: "is_isomorphism f \<Longrightarrow> is_monic f"
    unfolding defs using cat by (smt fl isomorphism_has_unique_inverse)

  lemma isomorphisms_epic[cat]: "is_isomorphism f \<Longrightarrow> is_epic f"
    unfolding defs using cat by (smt fl isomorphism_has_unique_inverse)

  lemma "is_monic m \<and> is_epic m \<longrightarrow> is_isomorphism m" nitpick oops

  lemma composition_of_monics[cat]: "is_monic i \<Longrightarrow> is_monic j \<Longrightarrow> E (j \<cdot> i) \<Longrightarrow> is_monic (j \<cdot> i)"
    unfolding defs using cat fl by (smt (verit))

  lemma composition_monic_implies_first_monic[cat]: 
    "is_monic g \<Longrightarrow> is_monic (g \<cdot> s) \<Longrightarrow> E s \<Longrightarrow> is_monic s"
    unfolding defs using cat fl by (smt (verit))
    
end

end
theory Subobjects imports Monics_Epics 

begin

subsection \<open>Subobjects\<close>

context category
begin

  definition generalized_element :: "'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool" ("_ _-in _" [120,120,120] 120) where
    "x A-in B \<equiv> x:A\<rightarrow>B"

  definition is_subobject_of (infix "is'_subobject'_of" 120) where
    "i is_subobject_of A \<equiv> is_monic i \<and> cod i \<simeq> A"

  definition member_of (infix "member'_of" 120) where
    "x member_of i \<equiv> (x (dom x)-in (cod i)) \<and> (\<exists>h. h:(dom x)\<rightarrow>(dom i) \<and> i\<cdot>h \<simeq> x)"

  definition included_in :: "'a \<Rightarrow> 'a \<Rightarrow> bool" (infix "included'_in" 120) where
    "i included_in j \<equiv> i is_subobject_of (cod j) \<and> j is_subobject_of (cod j) \<and> (\<exists>s. E s \<and> j \<cdot> s = i)"

  definition equivalent_to :: "'a \<Rightarrow> 'a \<Rightarrow> bool" (infix "equivalent'_to" 120) where
    "i equivalent_to j \<equiv> i included_in j \<and> j included_in i"

  declare generalized_element_def[defs] is_subobject_of_def[defs] member_of_def[defs] 
          included_in_def[defs] equivalent_to_def[defs]

  lemma included_in_member_of[cat]:
    assumes "i is_subobject_of (cod j)" and "j is_subobject_of (cod j)" 
    shows "i included_in j \<longleftrightarrow> (\<forall>m. m member_of i \<longrightarrow> m member_of j)"
    apply auto
    subgoal for m unfolding defs using fl cat by smt
    subgoal using assms unfolding defs using fl cat by smt
    done

  lemma equivalent_to_member_of[cat]:
    assumes "i is_subobject_of (cod j)" and "j is_subobject_of (cod j)" 
    shows "i equivalent_to j \<longleftrightarrow> (\<forall>m. m member_of i \<longleftrightarrow> m member_of j)"
    using assms included_in_member_of equivalent_to_def fl is_subobject_of_def by auto

end

end
theory Square_Diagrams imports "../Category"

begin

context category 
begin

named_theorems squares

text \<open>Convention: The labels of vertical arrows are always written on the outside or if the arrow
      is in the center of the diagram on the left. \<close>

definition rd_square_commutes :: "'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool" where
  "rd_square_commutes f h k g \<equiv> h \<cdot> f \<simeq> k \<cdot> g"

declare rd_square_commutes_def[squares]

abbreviation square_rd_diagram :: "'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool"
 ("square'_commutes // \<box> \<midarrow>_\<rightarrow> \<box> // _\<down> \<down>_ // \<box> \<midarrow>_\<rightarrow> \<box>") where
  "square_commutes
     \<box> \<midarrow>g\<rightarrow> \<box> 
    f\<down>      \<down>k
     \<box> \<midarrow>h\<rightarrow> \<box>  
  \<equiv> rd_square_commutes f h k g"

abbreviation double_square_rd_diagram :: "'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool" 
 ("double'_square'_commutes // \<box> \<midarrow>_\<rightarrow> \<box> \<midarrow>_\<rightarrow> \<box> // _\<down> _\<down> \<down>_ // \<box> \<midarrow>_\<rightarrow> \<box> \<midarrow>_\<rightarrow> \<box>") where
  "double_square_commutes
     \<box> \<midarrow>k\<rightarrow> \<box> \<midarrow>m\<rightarrow> \<box>
    h\<down>      i\<down>      \<down>j
     \<box> \<midarrow>f\<rightarrow> \<box> \<midarrow>g\<rightarrow> \<box> 
  \<equiv> square_commutes
     \<box> \<midarrow>k\<rightarrow> \<box>
    h\<down>       \<down>i
     \<box> \<midarrow>f\<rightarrow> \<box>
  \<and> square_commutes
     \<box> \<midarrow>m\<rightarrow> \<box>
    i\<down>      \<down>j
     \<box> \<midarrow>g\<rightarrow> \<box>"

lemma outer_square_commutes[squares]:
  assumes "double_square_commutes
              \<box> \<midarrow>k\<rightarrow> \<box> \<midarrow>m\<rightarrow> \<box>
             h\<down>      i\<down>       \<down>j
              \<box> \<midarrow>f\<rightarrow> \<box> \<midarrow>g\<rightarrow> \<box>"
  shows "square_commutes
         \<box> \<midarrow> m\<cdot>k \<rightarrow> \<box>
        h\<down>          \<down>j
         \<box> \<midarrow> g\<cdot>f \<rightarrow> \<box>"
  using assms unfolding squares using cat_ax fl by (smt rd_square_commutes_def)

end

end
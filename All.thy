theory All imports 

  Category
  Functors

  "Constructions/Isomorphisms"
  "Constructions/Monics_Epics"
  "Constructions/Subobjects"
  "Constructions/Finals_Initials"
  "Constructions/Products"
  "Constructions/Coproducts"
  "Constructions/Equalizers"
  "Constructions/Pullbacks"
  "Constructions/Exponentials"
  "Constructions/Limits"

  "Utilities/Square_Diagrams"

  "Special_Categories/Binary_Product_Category"
  "Special_Categories/Cartesian_Category"
  "Special_Categories/Cartesian_Closed_Category"
  "Special_Categories/Topos"

  "Category_Instantiations/Category_Of_Categories"
  "Category_Instantiations/Category_Of_Posets"
  "Category_Instantiations/Category_Of_Sets"
  "Category_Instantiations/Binary_Product_Category_Of_Sets"
  "Category_Instantiations/Binary_Product_Coproduct_Category_Of_Lattices"
  "Category_Instantiations/Category_Of_Categories"

begin

end
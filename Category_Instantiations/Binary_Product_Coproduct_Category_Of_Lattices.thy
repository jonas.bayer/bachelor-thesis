theory Binary_Product_Coproduct_Category_Of_Lattices 
  imports Category_Of_Posets "../Special_Categories/Binary_Product_Coproduct_Category"
  (* This theory imports the proof that Posets form a category.  *)

abbrevs rel = \<preceq> and
        trel = "\<preceq>\<^sub>T" and
        meet = "\<^bold>\<and>" and
        join = "\<^bold>\<or>"

begin

subsection \<open>Definition of bounds\<close>

  context poset begin
    definition upper_bound :: "'a \<Rightarrow> 'a set \<Rightarrow> bool" (infix "upper'_bound'_of" 50) where
      "u upper_bound_of S \<equiv> \<forall>s\<in>S. s \<preceq> u"
      
    definition lower_bound :: "'a \<Rightarrow> 'a set \<Rightarrow> bool" (infix "lower'_bound'_of" 50) where
      "l lower_bound_of S \<equiv> \<forall>s\<in>S. l \<preceq> s"
    
    definition least_upper_bound :: "'a \<Rightarrow> 'a set \<Rightarrow> bool" 
      (infix "least'_upper'_bound'_of" 50) where
      "u least_upper_bound_of S \<equiv> u upper_bound_of S \<and> (\<forall>x. x upper_bound_of S \<longrightarrow> u \<preceq> x)"
    
    definition greatest_lower_bound :: "'a \<Rightarrow> 'a set \<Rightarrow> bool" 
      (infix "greatest'_lower'_bound'_of" 50) where
      "l greatest_lower_bound_of S \<equiv> l lower_bound_of S \<and> (\<forall>x. x lower_bound_of S \<longrightarrow> x \<preceq> l)"
  end

subsection \<open>Semilattices\<close>

  locale join_semilattice = poset +
    fixes join :: "'a \<Rightarrow> 'a \<Rightarrow> 'a" (infix "\<^bold>\<or>" 120)
    assumes join_property: "a \<^bold>\<or> b least_upper_bound_of {a, b}"
  begin
    
    lemma monotonicity: "a\<^sub>1 \<preceq> a\<^sub>2 \<Longrightarrow> b\<^sub>1 \<preceq> b\<^sub>2 \<Longrightarrow> a\<^sub>1 \<^bold>\<or> b\<^sub>1 \<preceq> a\<^sub>2 \<^bold>\<or> b\<^sub>2" 
      using join_property unfolding least_upper_bound_def upper_bound_def 
      by (smt doubleton_eq_iff insertE insertI1 singleton_iff transitivity)
  
  end
  
  locale meet_semilattice = poset +
    fixes meet :: "'a \<Rightarrow> 'a \<Rightarrow> 'a" (infix "\<^bold>\<and>" 120)
    assumes meet_property: "a \<^bold>\<and> b greatest_lower_bound_of {a, b}"
  begin
  
    lemma monotonicity: "a\<^sub>1 \<preceq> a\<^sub>2 \<Longrightarrow> b\<^sub>1 \<preceq> b\<^sub>2 \<Longrightarrow> a\<^sub>1 \<^bold>\<and> b\<^sub>1 \<preceq> a\<^sub>2 \<^bold>\<and> b\<^sub>2" 
      using meet_property unfolding greatest_lower_bound_def lower_bound_def 
      by (smt doubleton_eq_iff insertE insertI1 singleton_iff transitivity)
  
  end

subsection \<open>Lattices\<close>

  locale lattice = join_semilattice + meet_semilattice
  begin

  lemma distributive_law: "x \<^bold>\<and> (y \<^bold>\<or> z) = (x \<^bold>\<and> y) \<^bold>\<or> (x \<^bold>\<and> z)"
    nitpick (* Finds the N\<^sub>5 lattice after a few milliseconds *)
    oops

  end

subsection \<open>Proving that lattices form a category with binary products\<close>

  context lattice 
  begin

  text \<open>Given a lattice one defines the category product as the meet; more concisely the identity
        on the meet as there are no objects. \<close>

  definition lattice_product :: "'a posetmap \<Rightarrow> 'a posetmap \<Rightarrow> 'a posetmap" (infix "\<^bold>\<times>" 120) where
    "A \<^bold>\<times> B \<equiv> if (is_object A \<and> is_object B) 
              then Id ((pdom A) \<^bold>\<and> (pdom B)) 
              else *"
  
  text \<open>The projections are then simply arrows from this product to one of the factors. This 
        works as A \<and> B \<preceq> A. \<close>
  
  definition lattice_projection1 :: "'a posetmap \<Rightarrow> 'a posetmap \<Rightarrow> 'a posetmap" ("\<pi>\<^sub>1") where
    "\<pi>\<^sub>1 A B \<equiv> if (is_object A \<and> is_object B)
              then Posetmap (pdom (A \<^bold>\<times> B)) (pdom A)
              else *"

  definition lattice_projection2 :: "'a posetmap \<Rightarrow> 'a posetmap \<Rightarrow> 'a posetmap" ("\<pi>\<^sub>2") where
    "\<pi>\<^sub>2 A B \<equiv> if (is_object A \<and> is_object B)
              then Posetmap (pdom (A \<^bold>\<times> B)) (pdom B)
              else *"

  text \<open>The following lines activate more convenient notation. \<close>

  notation is_object ("is'_type")
  notation arrow ("_:_\<rightarrow>_" [120,120,120] 119) 
  notation is_wedge ("_\<leftarrow>_\<midarrow> _ \<midarrow>_\<rightarrow>_" [120,120,120,120,120] 120)
  notation is_product ("is'_product _\<leftarrow>_\<midarrow> _ \<midarrow>_\<rightarrow>_" [120,120,120,120,120] 120) 

  text \<open>Two simple lemmas useful in the instation proofs. \<close>

  lemma posetmap_is_object: "is_object A \<longleftrightarrow> (A = Id (pdom A))"
    by (metis existing_identity_def inexistent_posetmap_def is_object_def posetmap.distinct(1) 
        posetmap.sel(2) posetmap_domain_def posetmap_existence_def posetmap_identity_def 
        reflexivity)

  lemma is_object_Id[simp]: "is_object (Id x)"
    using posetmap_is_object by (simp add: posetmap_identity_def)

  text \<open>Now one can show that the poset category with the additional structure of a lattice indeed
        forms a category with binary products. \<close>

  interpretation binary_product_category posetmap_existence 
                                 posetmap_domain posetmap_codomain 
                                 posetmap_composition inexistent_posetmap
                                 lattice_product lattice_projection1 lattice_projection2
  proof (unfold_locales)
                
  text \<open>Some simple axioms: \<close>

  show "E (A \<^bold>\<times> B) \<longrightarrow> is_object A \<and> is_object B" for A B :: "'a posetmap" 
    using lattice_product_def by (simp add: posetmap_existence_def)
  show "E (\<pi>\<^sub>1 A B) \<longrightarrow> is_object A \<and> is_object B" for A B :: "'a posetmap" 
    by (simp add: lattice_projection1_def posetmap_existence_def)
  show "E (\<pi>\<^sub>2 A B) \<longrightarrow> is_object A \<and> is_object B" for A B :: "'a posetmap"
    by (simp add: lattice_projection2_def posetmap_existence_def)

  text \<open>The product property: \<close>

  show "is_object A \<and> is_object B \<longrightarrow> is_product A\<leftarrow>\<pi>\<^sub>1 A B\<midarrow> A \<^bold>\<times> B \<midarrow>\<pi>\<^sub>2 A B\<rightarrow>B" for A B
    proof (auto)
      assume "is_object A" and "is_object B"

      hence "E A" and "E B" 
        by (simp_all add: types_exist) 
      hence "E (A \<^bold>\<times> B)" 
        by (simp add: \<open>is_object A\<close> \<open>is_object B\<close> inexistent_posetmap_def lattice_product_def 
            posetmap_existence_def posetmap_identity_def reflexivity) 
 
      have "E (\<pi>\<^sub>1 A B)" and "E (\<pi>\<^sub>2 A B)" 
        unfolding lattice_projection1_def lattice_projection2_def lattice_product_def
        using \<open>is_object A\<close> \<open>is_object B\<close> posetmap_identity_def posetmap_existence_def apply simp_all
        using greatest_lower_bound_def inexistent_posetmap_def lower_bound_def meet_property 
        by auto
 
      have is_wedge: "A\<leftarrow>(\<pi>\<^sub>1 A B)\<midarrow>(A \<^bold>\<times> B)\<midarrow>(\<pi>\<^sub>2 A B)\<rightarrow>B" 
        by (smt \<open>E (A \<^bold>\<times> B)\<close> \<open>E (\<pi>\<^sub>1 A B)\<close> \<open>E (\<pi>\<^sub>2 A B)\<close> \<open>E A\<close> \<open>E B\<close> \<open>is_object A\<close> \<open>is_object B\<close> arrow_def 
            is_object_def is_wedge_def lattice_product_def lattice_projection1_def 
            lattice_projection2_def posetmap.sel(1) posetmap.sel(2) posetmap_codomain_def 
            posetmap_domain_def posetmap_identity_def posetmap_is_object)

      have product_property: "A \<leftarrow>h\<midarrow> T \<midarrow>k\<rightarrow> B 
                              \<Longrightarrow> (\<exists>!u. \<pi>\<^sub>1 A B \<cdot> u \<simeq> h \<and> \<pi>\<^sub>2 A B \<cdot> u \<simeq> k)" for h T k
      proof -
        assume "A \<leftarrow>h\<midarrow> T \<midarrow>k\<rightarrow> B "
        hence "E h" and "E k" and "h:T\<rightarrow> A" and "k:T\<rightarrow>B"
          unfolding is_wedge_def arrow_def is_object_def 
          using cat_ax fl by blast+
        
        hence "pcod T = pdom h" and "pcod T = pdom k"
          by (smt arrow_def posetmap.sel posetmap_domain_def posetmap_identity_def fl)+
  
        have "is_object T"
          using \<open>k:T\<rightarrow>B\<close> arrow_implies_is_object by auto
  
        define u where "u \<equiv> Posetmap (pcod T) (pdom (A \<^bold>\<times> B))"
  
        have "pcod T \<preceq> pdom A" 
          using existing_identity_def \<open>h:T\<rightarrow>A\<close> \<open>is_object A\<close> \<open>pcod T = pdom h\<close> 
                posetmap_codomain_def posetmap_identity_def arrow_def 
                posetmap.sel(2) posetmap_existence_def is_object_def local.lattice_axioms 
          by fastforce
  
        moreover have "pcod T \<preceq> pdom B"
          using \<open>k:T\<rightarrow>B\<close> \<open>pcod T = pdom k\<close> arrow_def existing_identity_def posetmap_codomain_def 
                posetmap_existence_def posetmap_identity_def by auto
  
        ultimately have "pcod T lower_bound_of {pdom B, pdom A}"
          unfolding lower_bound_def by auto
  
        hence "E u" 
          by (smt \<open>is_object A\<close> \<open>is_object B\<close> greatest_lower_bound_def inexistent_posetmap_def 
              insert_commute lattice_product_def meet_property poset.posetmap_existence_def 
              poset_axioms posetmap.distinct(1) posetmap.sel(1) posetmap.sel(2) 
              posetmap_identity_def u_def)
   
        hence u_map: "u:T\<rightarrow>(A \<^bold>\<times> B)" 
          by (metis \<open>is_object T\<close> arrow_def arrow_implies_is_object existing_identity_def is_wedge 
              is_wedge_def posetmap.sel(1) posetmap.sel(2) posetmap_codomain_def posetmap_domain_def 
              posetmap_identity_def posetmap_is_object types_exist u_def)
  
        have comp1: "\<pi>\<^sub>1 A B \<cdot> u \<simeq> h" 
          by (metis \<open>h:T\<rightarrow>A\<close> \<open>pcod T = pdom h\<close> arrow_def composition_existence existing_identity_def 
              inexistent_posetmap_def is_wedge is_wedge_def poset.posetmap_codomain_def 
              poset.posetmap_composition_def poset_axioms posetmap.exhaust_sel posetmap.sel(1) 
              posetmap_existence_def posetmap_identity_def u_def u_map)
  
        have comp2: "\<pi>\<^sub>2 A B \<cdot> u \<simeq> k"
          by (metis \<open>k:T\<rightarrow>B\<close> \<open>pcod T = pdom k\<close> arrow_def composition_existence existing_identity_def 
              inexistent_posetmap_def is_wedge is_wedge_def poset.posetmap_codomain_def 
              poset.posetmap_composition_def poset_axioms posetmap.exhaust_sel posetmap.sel(1) 
              posetmap_existence_def posetmap_identity_def u_def u_map)
  
        have uniqueness: "\<pi>\<^sub>1 A B \<cdot> v \<simeq> h \<Longrightarrow> \<pi>\<^sub>2 A B \<cdot> v \<simeq> k \<Longrightarrow> u = v" for v 
          by (metis \<open>pcod T = pdom h\<close> existing_identity_def inexistent_element 
              inexistent_posetmap_def lattice_projection1_def poset.composable_def poset_axioms 
              posetmap.collapse posetmap.sel(1) posetmap_composition_def u_def)
  
        show "(\<exists>!u. \<pi>\<^sub>1 A B \<cdot> u \<simeq> h \<and> \<pi>\<^sub>2 A B \<cdot> u \<simeq> k)" 
          using comp1 comp2 uniqueness by auto
      qed

      show "is_product A\<leftarrow>\<pi>\<^sub>1 A B\<midarrow> A \<^bold>\<times> B \<midarrow>\<pi>\<^sub>2 A B\<rightarrow>B" 
        by (simp add: is_product_def is_wedge product_property)
    qed   

  qed
  
subsection \<open>One can prove the analogous statement for coproducts with joins:  \<close>

  definition lattice_coproduct :: "'a posetmap \<Rightarrow> 'a posetmap \<Rightarrow> 'a posetmap" (infix "\<squnion>" 120) where
    "A \<squnion> B \<equiv> if (is_object A \<and> is_object B) 
              then Id ((pdom A) \<^bold>\<or> (pdom B)) 
              else *"
 
  definition lattice_insertion1 :: "'a posetmap \<Rightarrow> 'a posetmap \<Rightarrow> 'a posetmap" ("\<iota>\<^sub>1") where
    "\<iota>\<^sub>1 A B \<equiv> if (is_object A \<and> is_object B)
              then Posetmap (pdom A) (pdom (A \<squnion> B))
              else *"

  definition lattice_insertion2 :: "'a posetmap \<Rightarrow> 'a posetmap \<Rightarrow> 'a posetmap" ("\<iota>\<^sub>2") where
    "\<iota>\<^sub>2 A B \<equiv> if (is_object A \<and> is_object B)
              then Posetmap (pdom B) (pdom (A \<squnion> B))
              else *"

  notation is_cowedge ("_\<midarrow>_\<rightarrow> _ \<leftarrow>_\<midarrow>_" [120,120,120,120,120] 120)
  notation is_coproduct ("is'_coproduct _\<midarrow>_\<rightarrow> _ \<leftarrow>_\<midarrow>_" [120,120,120,120,120] 120) 

  interpretation binary_coproduct_category posetmap_existence 
                                 posetmap_domain posetmap_codomain 
                                 posetmap_composition inexistent_posetmap
                                 lattice_coproduct lattice_insertion1 lattice_insertion2
  proof (unfold_locales)
                
  text \<open>Simple axioms: \<close>

  show "E (A \<squnion> B) \<longrightarrow> is_object A \<and> is_object B" for A B :: "'a posetmap" 
    using lattice_coproduct_def by (simp add: posetmap_existence_def)
  show "E (\<iota>\<^sub>1 A B) \<longrightarrow> is_object A \<and> is_object B" for A B :: "'a posetmap" 
    by (simp add: lattice_insertion1_def posetmap_existence_def)
  show "E (\<iota>\<^sub>2 A B) \<longrightarrow> is_object A \<and> is_object B" for A B :: "'a posetmap"
    by (simp add: lattice_insertion2_def posetmap_existence_def)

  text \<open>The coproduct property. The proof is analogous to the first proof with meets. \<close>

  show "is_object A \<and> is_object B \<longrightarrow> is_coproduct A\<midarrow>\<iota>\<^sub>1 A B\<rightarrow> A \<squnion> B \<leftarrow>\<iota>\<^sub>2 A B\<midarrow> B" for A B
    proof (auto)
      assume "is_object A" and "is_object B"

      hence "E A" and "E B" 
        by (simp_all add: types_exist) 
      hence "E (A \<squnion> B)" 
        by (simp add: \<open>is_object A\<close> \<open>is_object B\<close> inexistent_posetmap_def lattice_coproduct_def 
            posetmap_existence_def posetmap_identity_def reflexivity) 
 
      have "E (\<iota>\<^sub>1 A B)" and "E (\<iota>\<^sub>2 A B)" 
        unfolding lattice_insertion1_def lattice_insertion2_def lattice_coproduct_def
        using \<open>is_object A\<close> \<open>is_object B\<close> posetmap_identity_def posetmap_existence_def apply simp_all
        using least_upper_bound_def inexistent_posetmap_def upper_bound_def join_property 
        by auto
 
      have is_wedge: "A\<midarrow>(\<iota>\<^sub>1 A B)\<rightarrow>(A \<squnion> B)\<leftarrow>(\<iota>\<^sub>2 A B)\<midarrow>B" 
        by (smt \<open>E (A \<squnion> B)\<close> \<open>E (\<iota>\<^sub>1 A B)\<close> \<open>E (\<iota>\<^sub>2 A B)\<close> \<open>E A\<close> \<open>E B\<close> \<open>is_object A\<close> \<open>is_object B\<close> arrow_def 
            is_cowedge_def is_object_def lattice_coproduct_def lattice_insertion1_def 
            lattice_insertion2_def posetmap.sel(1) posetmap.sel(2) posetmap_codomain_def 
            posetmap_domain_def posetmap_identity_def posetmap_is_object)

      have coproduct_property: "A \<midarrow>h\<rightarrow> T \<leftarrow>k\<midarrow> B 
                              \<Longrightarrow> (\<exists>!u. u \<cdot> \<iota>\<^sub>1 A B \<simeq> h \<and> u \<cdot> \<iota>\<^sub>2 A B \<simeq> k)" for h T k
      proof -
        assume "A \<midarrow>h\<rightarrow> T \<leftarrow>k\<midarrow> B"
        hence "E h" and "E k" and "h:A\<rightarrow> T" and "k:B\<rightarrow>T"
          unfolding is_cowedge_def arrow_def is_object_def 
          using cat_ax fl by blast+
        
        hence "pdom T = pcod h" and "pdom T = pcod k"
          using arrow_def existing_identity_def posetmap_codomain_def posetmap_identity_def 
          by auto

        have "is_object T"
          using \<open>k:B\<rightarrow>T\<close> arrow_implies_is_object by auto
  
        define u where "u \<equiv> Posetmap (pdom (A \<squnion> B)) (pcod T)"
  
        have "pdom A \<preceq> pcod T" 
          using existing_identity_def \<open>h:A\<rightarrow>T\<close> \<open>is_object A\<close> \<open>pdom T = pcod h\<close> 
                posetmap_domain_def posetmap_identity_def arrow_def \<open>is_object T\<close>
                posetmap.sel(2) posetmap_existence_def is_object_def local.lattice_axioms 
          by fastforce
  
        moreover have "pdom B \<preceq> pcod T"
          using \<open>k:B\<rightarrow>T\<close> \<open>pdom T = pcod k\<close> arrow_def existing_identity_def posetmap_domain_def 
                posetmap_existence_def posetmap_identity_def 
          using \<open>is_object T\<close> posetmap_is_object by auto
  
        ultimately have "pcod T upper_bound_of {pdom A, pdom B}"
          unfolding upper_bound_def by auto
  
        hence "E u" 
          unfolding u_def posetmap_existence_def apply simp
          unfolding lattice_coproduct_def
          using \<open>is_object A\<close> \<open>is_object B\<close> apply simp 
          unfolding posetmap_identity_def inexistent_posetmap_def apply simp
          by (meson join_property poset.least_upper_bound_def poset_axioms)

        hence u_map: "u:(A\<squnion>B)\<rightarrow>T" 
          by (metis \<open>is_object T\<close> arrow_def arrow_implies_is_object existing_identity_def is_cowedge_def 
              is_wedge posetmap.sel(1) posetmap.sel(2) posetmap_codomain_def posetmap_domain_def 
              posetmap_identity_def posetmap_is_object types_exist u_def)
  
        have comp1: "u \<cdot> \<iota>\<^sub>1 A B \<simeq> h" 
          by (smt \<open>E (\<iota>\<^sub>1 A B)\<close> \<open>h:A\<rightarrow>T\<close> arrow_def composable_def existing_identity_def 
              kleene_equality_def lattice.lattice_insertion1_def local.lattice_axioms 
              poset.posetmap_composition_def poset_axioms posetmap.sel(1) posetmap.sel(2) u_map
              posetmap_codomain_def posetmap_existence_def posetmap_identity_def right_compose_id )
  
        have comp2: "u \<cdot> \<iota>\<^sub>2 A B \<simeq> k"
          by (smt \<open>E (\<iota>\<^sub>2 A B)\<close> \<open>k:B\<rightarrow>T\<close> arrow_def composable_def existing_identity_def 
              kleene_equality_def lattice.lattice_insertion2_def local.lattice_axioms 
              poset.posetmap_composition_def poset_axioms posetmap.sel(1) posetmap.sel(2) u_map
              posetmap_codomain_def posetmap_existence_def posetmap_identity_def right_compose_id)
  
        have uniqueness: "v \<cdot> \<iota>\<^sub>1 A B \<simeq> h \<Longrightarrow> v \<cdot> \<iota>\<^sub>2 A B \<simeq> k \<Longrightarrow> u = v" for v 
          by (metis comp1 composable_def existing_identity_def inexistent_posetmap_def 
              poset.posetmap_composition_def poset_axioms posetmap.collapse posetmap.sel(2) 
              posetmap_existence_def)
  
        show "(\<exists>!u. u \<cdot> \<iota>\<^sub>1 A B \<simeq> h \<and> u \<cdot> \<iota>\<^sub>2 A B \<simeq> k)" 
          using comp1 comp2 uniqueness by auto
      qed

      show "is_coproduct A\<midarrow>\<iota>\<^sub>1 A B\<rightarrow> A \<squnion> B \<leftarrow>\<iota>\<^sub>2 A B\<midarrow>B" 
        by (simp add: is_coproduct_def is_wedge coproduct_property)
    qed   

  qed

  text \<open>One can conclude that lattices indeed have binary products and coproducts: \<close>

  interpretation binary_product_coproduct_category posetmap_existence
                                                   posetmap_domain posetmap_codomain
                                                   posetmap_composition inexistent_posetmap
                                                   lattice_product lattice_projection1
                                                                   lattice_projection2
                                                   lattice_coproduct lattice_insertion1
                                                                     lattice_insertion2
    by (unfold_locales)

  text \<open>With the structure of a category with binary products and coproducts established one can
        check for distributivity counterexamples (on a category-theoretical level): \<close>

  lemma "local.distributive_category"
    nitpick[timeout = 1] oops

  text \<open>Isabelle does not succeed in finding one; likely the many added structures are simply too 
        elaborate. I suppose that using a better computer or giving it more time would produce an
        example. \<close>

subsection \<open>Distributive lattices are distributive categories: \<close>

  text \<open>Still, one can investigate how the distributive law within categories and the distributivity
        of a lattice are related. One says a lattice is distributive if the following statement
        holds: \<close>  

  definition distributive_lattice :: "bool" where
    "distributive_lattice \<equiv> \<forall>x y z. x \<^bold>\<and> (y \<^bold>\<or> z) = (x \<^bold>\<and> y) \<^bold>\<or> (x \<^bold>\<and> z)"

  text \<open>First one needs to explore isomorphisms in the poset category since distributivity is 
        naturally written as a a statement up to isomorphism within categories. \<close>

  notation kleene_isomorphic (infix "\<approx>" 56)

  lemma lattice_isomorphic_to: 
    assumes "is_object x"
    shows "isomorphic_to x y \<longleftrightarrow> x = y" 
  proof 
    assume "isomorphic_to x y"
    show "x = y"  
      by (metis \<open>isomorphic_to x y\<close> antisymmetry arrow_def composable_def existing_identity_def 
          inverse_of_def isomorphic_to_def isomorphism_arrow_def isomorphism_has_unique_inverse 
          posetmap_codomain_def posetmap_composition_def posetmap_domain_def posetmap_existence_def 
          types_exist)
  next
    assume "x = y"
    thus "isomorphic_to x y"
      by (metis assms composition_existence existing_identity_def inverse_of_def isomorphism_inverse
          is_isomorphism_def is_object_def isomorphic_to_def isomorphism_arrow_def 
          kleene_equality_def right_compose_id)
  qed

  text \<open>Then one can show that a lattice is distributive if and only if the corresponding 
        category is distributive. \<close>
  
  lemma "distributive_lattice \<longleftrightarrow> local.distributive_category"
  proof  
    assume "distributive_lattice"
    show "local.distributive_category" 
      unfolding local.distributive_category_def kleene_isomorphic_def distributivity_existence
      using \<open>distributive_lattice\<close> unfolding distributive_lattice_def 
      by (simp add: lattice_coproduct_def lattice_isomorphic_to lattice_product_def 
          posetmap_identity_def posetmap_is_object)  
  next
    assume "local.distributive_category"
    have "x \<^bold>\<and> (y \<^bold>\<or> z) = (x \<^bold>\<and> y) \<^bold>\<or> (x \<^bold>\<and> z)" for x y z
      proof - 
        have "Id x \<^bold>\<times> (Id y \<squnion> Id z) = (Id x \<^bold>\<times> Id y) \<squnion> (Id x \<^bold>\<times> Id z)"
          by (smt \<open>distributive_category\<close> distributive_category_def distributivity_existence 
              kleene_isomorphic_def lattice.lattice_isomorphic_to lattice_coproduct_def 
              lattice_product_def local.lattice_axioms posetmap.sel(1) posetmap_identity_def 
              posetmap_is_object)
        thus ?thesis 
          unfolding lattice_product_def lattice_coproduct_def apply simp
          unfolding posetmap_identity_def by simp
      qed
    thus "distributive_lattice"
      unfolding distributive_lattice_def by auto
  qed

end

end
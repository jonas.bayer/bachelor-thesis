theory Category imports Main

begin

declare [[ smt_solver = cvc4, smt_oracle = true, smt_timeout = 300]] 

section \<open>Free logic\<close>

locale free_logic = 
  fixes free_logic_existence :: "'a \<Rightarrow> bool" ("E") 

begin

  named_theorems fl
  
  definition kleene_equality (infixr "\<cong>" 56)
     where "x \<cong> y \<equiv> (E x \<or> E y) \<longrightarrow> x = y"  
  
  definition existing_identity (infixr "\<simeq>" 56)  
     where "x \<simeq> y \<equiv> E x \<and> E y \<and> x = y"
  
  declare kleene_equality_def[fl] existing_identity_def[fl]

end



text \<open>Define the following theorem lists for\<close>
  named_theorems defs  (* all definitions in the context of a category *)
  named_theorems cat   (* category axioms, theorems and lemmas *)
  named_theorems cat_ax (* category axioms *)

section \<open>The Definition of a category\<close>

locale category = free_logic +    
  fixes 
          domain :: "'a \<Rightarrow> 'a" ("dom _" [120] 120) and
          codomain :: "'a \<Rightarrow> 'a" ("cod _" [120] 120) and
          composition :: "'a \<Rightarrow> 'a  \<Rightarrow> 'a" (infix "\<cdot>" 120) and
          inexistent_element :: "'a" ("*")

  assumes  
          associativity[cat, cat_ax]:        "(x \<cdot> y) \<cdot> z \<cong> x \<cdot> (y \<cdot> z)" and
          right_compose_id[cat, cat_ax]:    "x \<cdot> (dom x) \<cong> x" and
          left_compose_id[cat, cat_ax]:     "(cod x) \<cdot> x \<cong> x" and 
   
          composition_existence[cat, cat_ax]: "E (x \<cdot> y) \<longleftrightarrow> dom x \<simeq> cod y" and
          dom_implies_existence[cat, cat_ax]:  "E (dom x) \<longrightarrow> E x" and
          cod_implies_existence[cat, cat_ax]:  "E (cod x) \<longrightarrow> E x" and
  
          inexistent_element[cat, cat_ax]:    "\<not> (E *)" 
begin

lemma "True" nitpick[satisfy, card=4] oops

subsection \<open>Basic definitions\<close>

  definition is_object ("is'_object") where 
    "is_object x \<equiv> x \<simeq> dom x" 

  definition arrow ("_:_\<rightarrow>_" [120,120,120] 119) where
    "f:A\<rightarrow>B \<equiv> dom f \<simeq> A \<and> cod f \<simeq> B"

  declare is_object_def[defs] arrow_def[defs]

  lemma types_exist[cat]: "is_object t \<longrightarrow> E t" unfolding existing_identity_def defs by simp

  lemma arrow_implies_is_object[cat]: "x:A\<rightarrow>B \<Longrightarrow> is_object A \<and> is_object B" 
    unfolding defs using cat fl by metis
  
  lemma arrow_composition[cat]: "f:A\<rightarrow>B \<Longrightarrow> g:B\<rightarrow>C \<Longrightarrow> g\<cdot>f:A\<rightarrow>C"
    unfolding fl arrow_def using cat fl by metis

end

end